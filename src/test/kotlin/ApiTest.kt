import httpRwquest.HttpRequest
import helpers.Json
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test



class ApiTest {

    @Test
    fun getUserByIdPositiveTest() {
        assertEquals(HttpRequest.get("https://reqres.in/api/users", 2).toJsonString(),
            Json.readFile("user.json").toJsonString())
    }

    @Test
    fun getUserByPageTest() {
        assertEquals(HttpRequest.get("https://reqres.in/api/users", mapOf("page" to "2")).toJsonString(),
            Json.readFile("list_users.json").toJsonString())
    }

    @Test
    fun getUserByIdNegativeTest() {
        assertEquals(HttpRequest.get("https://reqres.in/api/users", 23).toJsonString(),
            Json.fromString("{\"error\":404}").toJsonString())
    }

    @Test
    fun getResourcesAllTest() {
        assertEquals(HttpRequest.get("https://reqres.in/api/unknown").toJsonString(),
            Json.readFile("list_resources.json").toJsonString())
    }

    @Test
    fun getResourceByIdPositiveTest() {
        assertEquals(HttpRequest.get("https://reqres.in/api/unknown", 2).toJsonString(),
            Json.readFile("resource.json").toJsonString())
    }

    @Test
    fun getResourceByIdNegativeTest() {
        assertEquals(HttpRequest.get("https://reqres.in/api/unknown", 23).toJsonString(),
            Json.fromString("{\"error\":404}").toJsonString())
    }

    @Test
    fun createUserTest() {
        val response = HttpRequest.post("https://reqres.in/api/users",
            Json.fromString("{\"name\":\"morpheus\",\"job\":\"leader\"}"))
        assertEquals(response["name"], "morpheus")
        assertEquals(response["job"], "leader")
    }

    @Test
    fun putUpdateUserTest() {
        val response = HttpRequest.put("https://reqres.in/api/users", 2,
            Json.fromString("{\"name\":\"morpheus\",\"job\":\"zion resident\"}"))
        assertEquals(response["name"], "morpheus")
        assertEquals(response["job"], "zion resident")
    }

    @Test
    fun patchUpdateUserTest() {
        val response = HttpRequest.put("https://reqres.in/api/users", 2,
            Json.fromString("{\"name\":\"morpheus\",\"job\":\"zion resident\"}"))
        assertEquals(response["name"], "morpheus")
        assertEquals(response["job"], "zion resident")
    }

    @Test
    fun deleteUserTest() {
        assertEquals(HttpRequest.delete("https://reqres.in/api/users", 2).toJsonString(),
            Json.fromString("{\"error\":204}").toJsonString())
    }

    @Test
    fun registerUserPositiveTest() {
        val response = HttpRequest.post("https://reqres.in/api/register",
            Json.fromString("{\"email\":\"eve.holt@reqres.in\",\"password\":\"pistol\"}"))
        assertEquals(response["id"], 4)
        assertEquals(response["token"], "QpwL5tke4Pnpja7X4")
    }

    @Test
    fun registerUserNegativeTest() {
        assertEquals(HttpRequest.post("https://reqres.in/api/register",
                Json.fromString("{\"email\":\"sydney@fife\"}")).toJsonString(),
            Json.fromString("{\"error\":400}").toJsonString())
    }

    @Test
    fun loginPositiveTest() {
        assertEquals(HttpRequest.post("https://reqres.in/api/login",
                Json.fromString("{\"email\":\"eve.holt@reqres.in\",\"password\":\"cityslicka\"}"))["token"],
            "QpwL5tke4Pnpja7X4")
    }

    @Test
    fun loginNegativeTest() {
        assertEquals(HttpRequest.post("https://reqres.in/api/login",
                Json.fromString("{\"email\":\"peter@klaven\"}")).toJsonString(),
            Json.fromString("{\"error\":400}").toJsonString())
    }
}