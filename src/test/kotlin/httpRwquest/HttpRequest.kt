package httpRwquest

import com.beust.klaxon.JsonObject
import helpers.Json
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder

class HttpRequest {
    companion object {
        @JvmStatic
        fun get(url: String, data: Map<String, String>): JsonObject {
            val query = data.entries.stream()
                .map { (k, v) ->
                    "${URLEncoder.encode(k, "utf-8")}=${URLEncoder.encode(v.toString(), "utf-8")}"
                }
                ?.reduce { p1, p2 -> "$p1&$p2" }
                ?.orElse("")

            val conn = openConnection("$url?$query")
            conn.requestMethod = "GET"

            return getResponse(conn)
        }

        fun get(url: String, id: Int): JsonObject {

            val conn = openConnection("$url/$id")
            conn.requestMethod = "GET"

            return getResponse(conn)
        }

        fun get(url: String): JsonObject {

            val conn = openConnection(url)
            conn.requestMethod = "GET"

            return getResponse(conn)
        }

        @JvmStatic
        fun post(url : String, json : JsonObject): JsonObject {
            val conn = openConnection(url)
            conn.requestMethod = "POST"

            conn.setRequestProperty("Content-Type", "application/json")

            writeJson(conn, json)

            return getResponse(conn)
        }

        @JvmStatic
        fun patch(url : String, id : Int, json : JsonObject): JsonObject {
            val conn = openConnection("$url/$id")
            conn.requestMethod = "PATCH"

            conn.setRequestProperty("Content-Type", "application/json")

            writeJson(conn, json)

            return getResponse(conn)
        }

        @JvmStatic
        fun put(url : String, id : Int, json : JsonObject): JsonObject {
            val conn = openConnection("$url/$id")
            conn.requestMethod = "PUT"

            conn.setRequestProperty("Content-Type", "application/json")

            writeJson(conn, json)

            return getResponse(conn)
        }

        @JvmStatic
        fun delete(url : String, id : Int): JsonObject {
            val conn = openConnection("$url/$id")
            conn.requestMethod = "DELETE"

            return getResponse(conn)
        }

        private fun openConnection(url: String): HttpURLConnection {
            val conn = URL(url).openConnection() as HttpURLConnection
            conn.doOutput = true
            conn.useCaches = false
            conn.doInput = true
            conn.setRequestProperty("Accept", "application/json")

            return conn
        }

        private fun writeJson(conn : HttpURLConnection, json : JsonObject) {
            val outputStreamWriter = OutputStreamWriter(conn.outputStream)
            outputStreamWriter.write(json.toJsonString())
            outputStreamWriter.flush()
        }

        private fun getResponse(conn : HttpURLConnection): JsonObject {
            val responseCode = conn.responseCode
            if (responseCode == HttpURLConnection.HTTP_OK ||
                responseCode == HttpURLConnection.HTTP_CREATED) {
                val response = conn.inputStream.bufferedReader()
                    .use { it.readText() }
                conn.disconnect()
                return Json.fromString(response)
            } else {
                conn.disconnect()
                return Json.fromString("{\"error\":$responseCode}")
            }
        }
    }
}