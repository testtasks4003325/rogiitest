package helpers

import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser

class Json {
    companion object {
        @JvmStatic
        fun readFile(resourceName: String): JsonObject {
            val parser: Parser = Parser.default()
            val stringBuilder: StringBuilder = StringBuilder(this::class.java.classLoader.getResource(resourceName)!!.readText())
            return parser.parse(stringBuilder) as JsonObject
        }

        @JvmStatic
        fun fromString(string: String): JsonObject {
            val parser: Parser = Parser.default()
            val stringBuilder: StringBuilder = StringBuilder(string)
            return parser.parse(stringBuilder) as JsonObject
        }
    }
}